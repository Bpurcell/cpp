//============================================================================
// Name        : CFinalHmwk.cpp
// Author      : Bobby
// Version     :
// Copyright   : 
// Description :
//============================================================================

#include <algorithm>
#include <iostream>
#include <vector>
#include"RGBColor.h"
using namespace std;

int main() {
//	Create a vector of colors.  Also create the required iterator.
//	Read the color values from the keyboard until black is entered (0 0 0).  Store each color in the vector.
//	Sort the vector
//	Print the vector using the iterator.
	vector<RGBColor> RGBColors;
	int v1, v2, v3;
	cout << "Enter 3 values from 1-255" << endl;
	while (v1 != 0 && v2 != 0 && v3 != 0) {
		cin >> v1 >> v2 >> v3;
		cout << "You entered: " << v1 << endl << v2 << endl << v3 << endl;
		RGBColor c = RGBColor(v1, v2, v3);
		RGBColors.push_back(c);
		cout << c;
	}
	sort(RGBColors.begin(), RGBColors.end());

	for (std::vector<RGBColor>::iterator it = RGBColors.begin();
			it != RGBColors.end(); ++it)
		std::cout << ' ' << *it;
	std::cout << '\n';

	return 0;
}
